package config_test

import (
	config "config-go"
	"testing"
)

func TestLoadFile(t *testing.T) {
	_,err:= config.LoadFile("config.json")
	if err!=nil {
		t.Errorf("No se logro leer el archiv: %v", err)
	}
}

func TestLoadBytes(t *testing.T) {
	j:= [] byte(`{
		"db_server": "127.0.0.1",
		"db_name": "mydb",
		"db_port": "9090",
		"db_user": "hola",
		"db_password": "clave"
	}`)

	c:=config.Configuration{}
	err:= config.LoadBytes(j, &c)
	if err!=nil {
		t.Errorf(" No se logro leer los bytes: %v", err)
	}

}

func TestNew(t *testing.T) {
	_,err:= config.New("config.json")

	if err!=nil {
		t.Errorf(" No se logro cargar la configuración: %v", err)
	}
}

func TestConfiguration_Validate(t *testing.T) {
	c,err:= config.New("config.json")

	if err!=nil {
		t.Errorf(" No se logro cargar la configuración: %v", err)
	}
	err = c.Validate("db_server", "db_name", "db_port","db_user")
	if err!=nil {
		t.Errorf(" No se logro validar los campos de la configuración: %v", err)
	}
}

func TestConfiguration_Get(t *testing.T) {
	c,err:= config.New("config.json")

	if err!=nil {
		t.Errorf(" No se logro cargar la configuración: %v", err)
	}
	err = c.Validate("db_server", "db_name", "db_port","db_user")
	if err!=nil {
		t.Error(err)
	}

	_,err = c.Get("db_server")
	if err!=nil {
		t.Error(err)
	}
}
func TestConfiguration_GetStr(t *testing.T) {
	c,err:= config.New("config.json")

	if err!=nil {
		t.Errorf(" No se logro cargar la configuración: %v", err)
	}
	err = c.Validate("db_server", "db_name", "db_port","db_user")
	if err!=nil {
		t.Error(err)
	}

	_,err = c.GetStr("db_server")
	if err!=nil {
		t.Error(err)
	}
}


func TestConfiguration_GetInt(t *testing.T) {
	c,err:= config.New("config.json")
	if err!=nil {
		t.Errorf(" No se logro cargar la configuración: %v", err)
	}
	err = c.Validate("db_server", "db_name", "db_port","db_user")
	if err!=nil {
		t.Error(err)
	}
	_,err = c.GetInt("db_port")
	if err!=nil {
		t.Error(err)
	}
}

package config

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
)

var cnfg Configuration
/*
	Configuración Modelo que contiene mapa de
	String/interface para leer y almacenar la data
	de archivo de configurción tipo json
 */
type Configuration struct {
	data map[string]interface{}
}

func New(path string)	(*Configuration, error)  {
	b,err := LoadFile(path)
	if err!=nil {
		return nil,err
	}
	err = LoadBytes(b,&cnfg )
	if err!=nil {
		return nil,err
	}
	return &cnfg,nil
}
func LoadFile(path string) ([]byte,error){
	f,err := ioutil.ReadFile(path)
	if err!=nil {
		return f,err
	}
	return f,nil
}

func LoadBytes(d []byte, c *Configuration) error{
	err := json.Unmarshal(d,&c.data)
	if err!=nil {
		return err
	}
	return nil
}

func (c *Configuration) Validate(names ... string) error {
	for _,v:= range names{
		_,ok:=c.data[v]
		if !ok {
			 fmt.Printf("No existe el campo %s ",v)
			 return  errors.New(fmt.Sprintf("No existe el campo %s ",v))
		}
	}
	return nil
}

// Devuelve el valor del campo, si existe
func (c *Configuration) Get(name string) (interface{},error)  {
	v,ok:=c.data[name]

		if !ok {
			fmt.Printf("No existe el campo %v ",v)
			return  "", errors.New(fmt.Sprintf("No existe el campo %s ",name))
		}

	return v,nil
}

// Devuelve el valor del campo tipo String, si existe
func (c *Configuration) GetStr(name string) (string,error)  {
	var v, err = c.Get(name)
	if err!=nil {
		return  "", errors.New(fmt.Sprintf("Problemas con el campo %v ",err))
	}
	return v.(string),nil
}

// Devuelve el valor del campo tipo int, si existe
func (c *Configuration) GetInt(name string) (int,error)  {
	var v, err = c.Get(name)
	if err!=nil {
		return  0, errors.New(fmt.Sprintf("Problemas con el campo %v ",err))
	}
	return int(v.(float64)),nil
}
